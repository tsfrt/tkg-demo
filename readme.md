## Links
[artifactory](https://artifactory.ing.tsfrt.info/ui/login/)

[jenkins](https://jenkins.ing.tsfrt.info/)

[minio](https://minio.ing.tsfrt.info/minio/login)

## Setup

#add default storage class

k apply -f common/default-storage.yaml

#install contour

kubectl apply -f https://projectcontour.io/quickstart/contour.yaml

#setup cert manager (optional)

kubectl create namespace cert-manager
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.11.0/cert-manager.yaml
k apply -f cert-issuer.yaml

#depoy minio

helm repo add bitnami https://charts.bitnami.com/bitnami

k create ns minio

helm install minio bitnami/minio -n minio -f minio-values.yaml

#deploy jenkins

helm repo add bitnami https://charts.bitnami.com/bitnami

k create ns jenkins

helm install jenkins bitnami/jenkins -f jenkins-values.yaml -n jenkins


#deploy artificatory

k create ns artifactory

helm install artifactory jfrog/artifactory -f artifactory-values.yaml -n artifactory